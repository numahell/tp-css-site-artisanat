# Énoncé

## Ajouter du style

Lier le fichier `assets/css/styles.css`

## Changer la police de texte

1. Définir une police sans serif pour tout le texte
1. Couleur #333 pour toute la page
2. Mettre une couleur différente pour le titre principal
3. Changer la couleur du lien, et lui affecter une couleur de fond

## Définir la largeur maximale du contenu

* définir une largeur maximale pour le contenu principal pour toutes les pages

## Intégrer le pied de page

* couleur #F8F8F8 et fond #222222

## Positionner en ligne les éléments de la navigation

* Positionner en ligne les éléments de la navigation
    tips : `display: flex` et `list-item: none`
* Appliquer une couleur au survol des liens, y compris l'accueil
* Aligner le lien accueil et les autres liens

## Positionner les images des artisans

* Positionner l'image d'un artisan alternativement à droite ou à gauche du texte.

## Aérer les blocs des produits et les mettre en valeur

1. Mettre des marges et des espacements sur les blocs des produits.
2. Mettre une couleur de fond autre que celle de la page
3. (optionnel) Mettre une ombre sur les blocs

## Bonus : améliorer la navigation

* Positionner la navigation en fixe, avec une conleur de fond
* Décaler les liens de navigation à droite